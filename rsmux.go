package rsmux

import (
	"net/http"
	"strings"
)

type ResourceServeMux struct {
	http.ServeMux
	muxes map[string]*http.ServeMux
}

func (m *ResourceServeMux) HandleResource(path ResourcePath, rs Resource) {
	pattern := path[len(path)-1] + "/"
	handler := ResourceHandler{path, rs, m.pathServeMux(path)}
	m.pathServeMux(path[:len(path)-1]).Handle(pattern, &handler)
}

func (m *ResourceServeMux) pathServeMux(path ResourcePath) (pmux *http.ServeMux) {
	if key := strings.Join(path, "*"); key != "" {
		if m.muxes == nil {
			m.muxes = make(map[string]*http.ServeMux)
		}
		if mux, ok := m.muxes[key]; ok {
			return mux
		}
		pmux = http.NewServeMux()
		m.muxes[key] = pmux
		return

	}
	return &m.ServeMux
}
