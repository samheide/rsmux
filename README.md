# rsmux

`rsmux` is a multiplexer for HTTP requests written in Go. It is compatible with `http.ServeMux` and takes an additional method

```go
func (m *ResourceServeMux) HandleResource(path ResourcePath, rs Resource)
```

with the following types:

```go
type ResourcePath []string

type Resource interface {
	FetchIndividual(w http.ResponseWriter, r *http.Request)
	FetchCollection(w http.ResponseWriter, r *http.Request)
	CreateNew(w http.ResponseWriter, r *http.Request)
	UpdateExisting(w http.ResponseWriter, r *http.Request)
	RemoveExisting(w http.ResponseWriter, r *http.Request)
}
```

## Usage

Given a `ResourcePath{"/api/foo"}` value and `Resource` implementation the following REST resources and methods are registered by calling `HandleResource` func:

```
GET    /api/foo/{id} --> FetchIndividual
GET    /api/foo/     --> FetchCollection
POST   /api/foo/     --> CreateNew
PATCH  /api/foo/{id} --> UpdateExisting
DELETE /api/foo/{id} --> RemoveExisting
```

Nesting is supported, eg. `ResourcePath{"/api/foo", "/bar}` corresponds to:

```
GET    /api/foo/{foo-id}/bar/{bar-id} --> FetchIndividual
GET    /api/foo/{foo-id}/bar/         --> FetchCollection
...
```
