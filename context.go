package rsmux

import (
	"context"
	"net/http"
)

type ResourceVars map[Resource]string

type contextKey int

const varsKey contextKey = iota

func Vars(r *http.Request) ResourceVars {
	if val := r.Context().Value(varsKey); val != nil {
		return val.(ResourceVars)
	}
	return nil
}

func NewResourceVar(ctx context.Context, rs Resource, head string) context.Context {
	vars := make(ResourceVars)
	if val := ctx.Value(varsKey); val != nil {
		for k, v := range val.(ResourceVars) {
			vars[k] = v
		}
	}
	vars[rs] = head
	return context.WithValue(ctx, varsKey, vars)
}
