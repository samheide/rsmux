package rsmux

import (
	"net/http"
	"path"
	"strings"
)

type ResourcePath []string

type ResourceHandler struct {
	path ResourcePath
	rs   Resource
	mux  *http.ServeMux
}

func (handler *ResourceHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if head, tail, ok := handler.path.Traverse(r.URL.Path); ok {
		if head != "" {
			ctx := NewResourceVar(r.Context(), handler.rs, head)
			if tail != "/" {
				prefix := r.URL.Path[:strings.LastIndex(r.URL.Path, tail)]
				http.StripPrefix(prefix, http.HandlerFunc(func(w http.ResponseWriter, r2 *http.Request) {
					h, pattern := handler.mux.Handler(r2)
					if pattern != "" && strings.HasPrefix(r2.URL.Path, pattern) {
						h.ServeHTTP(w, r.WithContext(ctx))
						return
					}
					http.Error(w, "Not Found", http.StatusNotFound)
				})).ServeHTTP(w, r)
				return
			}
			switch r.Method {
			case http.MethodGet:
				handler.rs.FetchIndividual(w, r.WithContext(ctx))
			case http.MethodPatch:
				handler.rs.UpdateExisting(w, r.WithContext(ctx))
			case http.MethodDelete:
				handler.rs.RemoveExisting(w, r.WithContext(ctx))
			default:
				http.Error(w, "Method Not Allowed", http.StatusMethodNotAllowed)
			}
		} else {
			switch r.Method {
			case http.MethodGet:
				handler.rs.FetchCollection(w, r)
			case http.MethodPost:
				handler.rs.CreateNew(w, r)
			default:
				http.Error(w, "Method Not Allowed", http.StatusMethodNotAllowed)
			}
		}
	} else {
		http.Error(w, "Not Found", http.StatusNotFound)
	}
}

// ShiftPath splits off the first component of p, which will be cleaned of
// relative components before processing. head will never contain a slash and
// tail will always be a rooted path without trailing slash.
func ShiftPath(p string) (head, tail string) {
	p = path.Clean("/" + p)
	i := strings.Index(p[1:], "/") + 1
	if i <= 0 {
		return p[1:], "/"
	}
	return p[1:i], p[i:]
}

func ShiftPathPrefix(p, prefix string) (head, tail string, ok bool) {
	head, tail = ShiftPath(p)
	if prefix != "/" {
		if prefixHead, prefixTail := ShiftPath(prefix); prefixHead == head {
			return ShiftPathPrefix(tail, prefixTail)
		}
		return head, tail, false
	}
	return head, tail, true
}

func (path ResourcePath) Traverse(p string) (head, tail string, ok bool) {
	for _, prefix := range path {
		if head, tail, ok = ShiftPathPrefix(p, prefix); ok {
			p = tail
			continue
		}
		break
	}
	return
}
