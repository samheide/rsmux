package rsmux

import (
	"net/http"
)

type Resource interface {
	FetchIndividual(w http.ResponseWriter, r *http.Request)
	FetchCollection(w http.ResponseWriter, r *http.Request)
	CreateNew(w http.ResponseWriter, r *http.Request)
	UpdateExisting(w http.ResponseWriter, r *http.Request)
	RemoveExisting(w http.ResponseWriter, r *http.Request)
}
