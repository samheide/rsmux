package main

import (
	"fmt"
	"net/http"

	"gitlab.com/samheide/rsmux"
)

type TestResource struct {
	name string
}

func (e *TestResource) FetchIndividual(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "FetchIndividual %v=%v", e.name, rsmux.Vars(r)[e])
}

func (e *TestResource) FetchCollection(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "FetchCollection ", e.name)
}

func (e *TestResource) CreateNew(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "CreateNew ", e.name)
}

func (e *TestResource) UpdateExisting(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "UpdateExisting ", e.name)
}

func (e *TestResource) RemoveExisting(w http.ResponseWriter, r *http.Request) {
	http.Error(w, "Forbidden", http.StatusForbidden)
}
