package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"

	"gitlab.com/samheide/rsmux"
)

var (
	FooResource    = TestResource{"foo"}
	FooBarResource = TestResource{"bar"}
)

func main() {
	mux := new(rsmux.ResourceServeMux)
	mux.HandleResource(rsmux.ResourcePath{"/api/foo"}, &FooResource)
	mux.HandleResource(rsmux.ResourcePath{"/api/foo", "/bar"}, &FooBarResource)

	req, _ := http.NewRequest(http.MethodGet, "/api/foo/1/bar/2", nil)
	w := httptest.NewRecorder()
	mux.ServeHTTP(w, req)
	body, _ := ioutil.ReadAll(w.Body)
	fmt.Println(string(body))

	http.ListenAndServe(":8080", mux)
}
